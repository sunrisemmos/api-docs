# Game Login API
This API located [here](https://sunrise.games/api/login/alt/) supplies a token to be passed to the game client to be used for authentication.

## Arguments
`username` - The user's username.

`password` - The user's password.

`serverType` - Game to be played.

The server type can be one of the following:
* `Final Toontown`
* `Test Toontown 2012`
* `Toontown Japan 2010`
* `Toontown Brazil QA`

If not specified, will default to `Final Toontown`.

## Response
Response will be in JSON with the following data.

`success` (Boolean)

`errorCode` (Integer)

`token` (String)

`message` (String)

```json
{
  "success": true,
  "errorCode": -1,
  "token": "1234567890",
  "message": "Welcome back, Rocket!",
}
```

### Post-Success Login
When `success` is true, the user can use the `token` to play on one of our hosted games.

This is done via environment variables set before running the game process.

Below is a list of the required environment variables to boot the games we host.

Disney's Toontown Online (Toontown.exe):

* `GAME_SERVER`: set to `unite.sunrise.games:6667`
* `DOWNLOAD_SERVER`: set to `http://download.sunrise.games/launcher/`
* `LOGIN_TOKEN`: set to the `token` variable from above

In the case of Lego Universe, credentials are checked on the server side as the client doesn't use a login token.

Make sure you set these environment variables before starting the game.